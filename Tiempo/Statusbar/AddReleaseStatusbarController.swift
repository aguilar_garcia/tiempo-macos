//
//  AddReleaseStatusbarController.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 31.01.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import Cocoa

class AddReleaseStatusbarController: NSViewController, NSTextFieldDelegate {
    
    @IBOutlet weak var TextFieldTitle: NSTextField!
    
    @IBOutlet weak var releaseNameTextField: NSTextField!
    
    @IBAction func onButtonCancel(_ sender: Any) {
        closeWindow(sender)
    }
    
    @IBAction func onButtonOk(_ sender: Any) {
        if releaseNameTextField.stringValue != "" {
            _ = ReleaseManager.sharedInstance.addRelease(releaseName: releaseNameTextField!.stringValue)
            print("Entered releasename = \(releaseNameTextField.stringValue)")
            NotificationCenter.default.post(name: .reloadSidebar, object: managedObjectContext)
            NotificationCenter.default.post(name: .reloadStatusbar, object: managedObjectContext)
            NotificationCenter.default.post(name: .reloadReleaseTable, object: managedObjectContext)
            NotificationCenter.default.post(name: .itemChanged, object: managedObjectContext)
            closeWindow(sender)
            
        }
    }
    
    var managedObjectContext: NSManagedObjectContext?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //boxRelease.title = UserDefaults.standard.string(forKey: "Category")!
        let appDelegate = NSApplication.shared.delegate as! AppDelegate
        self.managedObjectContext = appDelegate.persistentContainer.viewContext
        // Do view setup here.
        releaseNameTextField.layer?.borderWidth = 1
        releaseNameTextField.layer?.borderColor = (CIColor.red as! CGColor)
        releaseNameTextField.delegate = self
        let backgroundColor = NSColor.controlBackgroundColor.cgColor
        let color = CALayer()
        color.backgroundColor = backgroundColor
        self.view.layer = color
    }

    
    func controlTextDidChange(_ obj: Notification) {
        if let textField = obj.object as? NSTextField, self.releaseNameTextField.identifier == textField.identifier {
            releaseNameTextField.backgroundColor = NSColor.darkGray
            self.TextFieldTitle.stringValue = textField.stringValue
            print("Changed text = \(textField.stringValue)\n")
        }
    }
    
    func closeWindow(_ sender: Any) {
        if let window = self.view.window?.windowController {
            window.close()
        } else {
            self.dismiss(sender)
        }
    }
    
}
