//
//  StatusbarSidebar.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 17.03.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import Cocoa

extension StatusbarViewController : NSOutlineViewDelegate, NSOutlineViewDataSource {
    

    func outlineViewSelectionDidChange(_ notification: Notification) {
        guard let outlineView = notification.object as? NSOutlineView else {
            return
        }
        statusbarSplitView.removeArrangedSubview(detailsViewItem)
        statusbarSplitView.addArrangedSubview(tableViewItem)
        statusbarTableView.reloadData()
        /*
        if let item = self.sidebarOutlineView.item(atRow: selectedIndex) as? Release {
            self.fillDetailsView(release: item)
        }
        if let item = self.sidebarOutlineView.item(atRow: selectedIndex) as? Ticket {
            print("ticket clicked")
            self.fillDetailsView(ticket: item)
        }
 */
    }
    
    func outlineView(_ outlineView: NSOutlineView, shouldShowOutlineCellForItem item: Any) -> Bool {
        return false
    }
    
    func tableView(_ tableView: NSTableView, shouldShowCellExpansionFor tableColumn: NSTableColumn?, row: Int) -> Bool {
        return false
    }
    
    func outlineView(_ outlineView: NSOutlineView, viewFor tableColumn: NSTableColumn?, item: Any) -> NSView? {
        var view: NSTableCellView?
        if let release = item as? Release {
            view = outlineView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "HeaderCell"), owner: self) as? NSTableCellView
            if let textField = view?.textField {
                textField.stringValue = release.releaseName!
            }
            if let imageView = view?.imageView {
                imageView.image = NSImage(named: .project)
            }
        } else if let ticket = item as? Ticket {
            view = outlineView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "DataCell"), owner: self) as? NSTableCellView
            if let textfield = view?.textField {
                textfield.stringValue = ticket.ticketname!
            }
            if let imageView = view?.imageView {
                imageView.image = NSImage(named: .ticket)
            }
        }
            return view
    }
    
    func outlineView(_ outlineView: NSOutlineView, numberOfChildrenOfItem item: Any?) -> Int {
        if let release = item as? Release {
            let releaseChilds = self.tickets.filter {
                return ($0 as Ticket).releaseId == release.releaseId
            }
            return releaseChilds.count
        }
        return releases.count
    }
    
    func outlineView(_ outlineView: NSOutlineView, isItemExpandable item: Any) -> Bool {
        if let release = item as? Release {
            let releaseChild = self.tickets.filter {
                return ($0 as Ticket).releaseId == release.releaseId
            }
            return releaseChild.count > 0
        }
        return false
    }
    
    func outlineView(_ outlineView: NSOutlineView, child index: Int, ofItem item: Any?) -> Any {
        if let release = item as? Release {
            let releaseChild = self.tickets.filter {
                return ($0 as Ticket).releaseId == release.releaseId
            }
            return releaseChild[index]
        }
        return releases[index]
    }
}
