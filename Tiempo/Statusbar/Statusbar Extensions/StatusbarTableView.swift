//
//  StatusbarTableView.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 17.03.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import Cocoa

extension StatusbarViewController: NSTableViewDelegate, NSTableViewDataSource {
        
    func numberOfRows(in tableView: NSTableView) -> Int {
        let selectedOutlineViewRow = sidebarOutlineView.selectedRow
        if let release = sidebarOutlineView.item(atRow: selectedOutlineViewRow) as? Release {
            return TicketManager.sharedInstance.getTicketsFromRelease(release: release).count
        } else if let ticket = sidebarOutlineView.item(atRow: selectedOutlineViewRow) as? Ticket {
            return TimeManager.sharedInstance.getTimesFromTicket(ticket: ticket).count
        }
        return 0
    }
    
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let selectedOutlineViewRow = sidebarOutlineView.selectedRow
        if let release = sidebarOutlineView.item(atRow: selectedOutlineViewRow) as? Release {
            let tickets = TicketManager.sharedInstance.getTicketsFromRelease(release: release)
            let ticket = tickets[row]
            if let cell = tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "customCell"), owner: self) as? StatusbarTableCellView {
                let loggedTime = TicketManager.sharedInstance.getEstimatedTime(ticket)
                cell.themeImage.image = NSImage(named: .ticket)
                cell.labelTitle.stringValue = ticket.ticketname!
                cell.labelLoggedDuration.stringValue = TimeConverter.sharedInstance.GetStringValueFromTime(time: Int(loggedTime))
                cell.labelDateTime.stringValue = "\(ticket.date ?? "") \(ticket.time ?? "")"
                return cell
            }
        } else if let ticket = sidebarOutlineView.item(atRow: selectedOutlineViewRow) as? Ticket {
            let times = TimeManager.sharedInstance.getTimesFromTicket(ticket: ticket)
            let time = times[row]
            if let cell = tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "customCell"), owner: self) as? StatusbarTableCellView {
                let loggedTime = time.duration
                cell.themeImage.image = NSImage(named: .time)
                cell.labelTitle.stringValue = TimeConverter.sharedInstance.GetStringValueFromTime(time: Int(loggedTime)) ?? ""
                cell.labelDateTime.stringValue = "\(time.date ?? "") \(time.created ?? "")"
                cell.labelLoggedDuration.stringValue = ""
                return cell
            }
        }
        return nil
    }
    
    
    func tableViewSelectionDidChange(_ notification: Notification) {
        guard let tableView = notification.object as? NSTableView else {
            return }
        
        if lastTableViewSelection != nil {
        let lastTableCellView = tableView.view(atColumn: 0, row: lastTableViewSelection, makeIfNecessary: false)
            if let view = lastTableCellView as? StatusbarTableCellView {
                view.moreButton.isHidden = true
            }
        }
        let rowselected = tableView.selectedRow
        if rowselected != -1 {
            lastTableViewSelection = rowselected
            let tableCellView = tableView.view(atColumn: 0, row: rowselected, makeIfNecessary: true)
            if let view = tableCellView as? StatusbarTableCellView {
                view.moreButton.isHidden = false
            }
        }
    }

    func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat {
        return 40
    }
}
