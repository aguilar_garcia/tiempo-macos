//
//  StatusbarTableCellView.swift
//  
//
//  Created by Juan Carlos Aguilar on 17.03.19.
//

import Cocoa

class StatusbarTableCellView: NSTableCellView {
    
    
    @IBOutlet weak var themeImage: NSImageView!
    
    @IBOutlet weak var moreButton: NSButton!
    
    @IBOutlet weak var labelLoggedDuration: NSTextField!
    
    @IBOutlet weak var labelTitle: NSTextField!
    
    @IBOutlet weak var labelDateTime: NSTextField!
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        // Drawing code here.
    }
    
    
    
}
