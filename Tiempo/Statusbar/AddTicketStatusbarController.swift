//
//  CreateNewEntryController.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 29.01.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import Cocoa

class AddTicketStatusbarController: NSViewController, NSTextFieldDelegate {
    
    @IBOutlet weak var buttonIsImportant: NSButton!
    
    @IBOutlet weak var TextFieldTitle: NSTextField!
    
    @IBOutlet weak var LblTicket: NSTextField!
    
    @IBOutlet weak var LblDetails: NSTextField!
    
    @IBOutlet weak var LblRelease: NSTextField!
    
    @IBOutlet weak var TxtTicketNumber: NSTextField!
    
    @IBOutlet weak var TxtDetails: NSTextField!
    
    @IBOutlet weak var selectReleasePopup: NSPopUpButton!
    
    @IBAction func ticketNumberChange(_ sender: Any) {
    }
    
    @IBAction func ticketDetailsChange(_ sender: Any) {
    }
    
    @IBOutlet weak var BtnStartNowOutlet: NSButton!
    
    @IBAction func BtnStartNow(_ sender: Any) {
//        if BtnStartNowOutlet.state == NSControl.StateValue.on {
//            setCurrentTime()
//        } else {
//            TxtStartTime.stringValue = ""
//        }
    }
    
    @IBAction func onIsImportant(_ sender: Any) {
        if (buttonIsImportant.state == .on) {
            buttonIsImportant.image = NSImage(named: .alarmOn)
        } else {
            buttonIsImportant.image = NSImage(named: .alarmOff)
        }
    }
    
    
    @IBAction func onButtonCancel(_ sender: Any) {
        closeWindow(sender)
    }
    
    var releases: [Release] = []
    var mainWindow: MainWindowController? = nil
    
    var managedObjectContext: NSManagedObjectContext?
    
    @IBAction func BtnCreateTicket(_ sender: Any) {
        if TxtTicketNumber.stringValue.count < 1 {
            TxtTicketNumber.backgroundColor = NSColor.red
            return
        }
        let releaseId = releases[self.selectReleasePopup.indexOfSelectedItem].releaseId
        _ = TicketManager.sharedInstance.addTicket(releaseId: releaseId!, ticketname: TxtTicketNumber.stringValue, details: TxtDetails.stringValue, isImportant: buttonIsImportant.state == .on)

        mainWindow?.mainSidebarViewController?.outlineView?.reloadData()
        NotificationCenter.default.post(name: .reloadSidebar, object: managedObjectContext)
        NotificationCenter.default.post(name: .reloadStatusbar, object: managedObjectContext)
        NotificationCenter.default.post(name: .reloadReleaseTable, object: managedObjectContext)
        NotificationCenter.default.post(name: .itemChanged, object: managedObjectContext)
        closeWindow(sender)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate = NSApplication.shared.delegate as! AppDelegate
        LblRelease.stringValue = UserDefaults.standard.string(forKey: "Category")!
        self.managedObjectContext = appDelegate.persistentContainer.viewContext
        TxtDetails.delegate = self
        TxtTicketNumber.delegate = self
        let releaseList = ReleaseManager.sharedInstance.getReleases()
        for release in releaseList {
            self.releases.append(release)
            self.selectReleasePopup.addItem(withTitle: release.releaseName!)
        }
        let backgroundColor = NSColor.controlBackgroundColor.cgColor
        let color = CALayer()
        color.backgroundColor = backgroundColor
        self.view.layer = color
        // Do view setup here.
    }
    
    func controlTextDidChange(_ obj: Notification) {
        if let textField = obj.object as? NSTextField, self.TxtDetails.identifier == textField.identifier {
            TxtDetails.backgroundColor = NSColor.darkGray
            print("Changed text = \(textField.stringValue)\n")
        }
        if let textField = obj.object as? NSTextField, self.TxtTicketNumber.identifier == textField.identifier {
            TxtTicketNumber.backgroundColor = NSColor.darkGray
            self.TextFieldTitle.stringValue = textField.stringValue
            print("Changed text = \(textField.stringValue)\n")
        }
    }
    
    func closeWindow(_ sender: Any) {
        if let window = self.view.window?.windowController {
            window.close()
        } else {
            self.dismiss(sender)
        }
    }
    
}
