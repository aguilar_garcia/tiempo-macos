//
//  StatusbarViewController.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 17.03.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import Cocoa

class StatusbarViewController: NSViewController, NSTextViewDelegate {

    @IBOutlet weak var statusbarSplitView: NSSplitView!
    
    @IBOutlet weak var sidebarOutlineView: NSOutlineView!
    
    @IBOutlet weak var detailsView: NSView!
    
    @IBOutlet weak var statusbarTableView: NSTableView!
    
    // Outlets for the Detail-View
    @IBOutlet weak var labelDetailsTitle: NSTextField!
    
    @IBOutlet weak var labelDetailsLoggedDuration: NSTextField!
    
    @IBOutlet weak var labelDetailsDateTimeCreated: NSTextField!
    
    @IBOutlet var labelDetailsDescription: NSTextView!
    
    @IBOutlet weak var copyLoggedTimeButton: NSButton!
    
    
    @IBAction func onCopyTitle(_ sender: Any) {
        pasteboard.clearContents()
        pasteboard.setString(labelDetailsTitle.stringValue, forType: .string)
    }
    
    @IBAction func onCopyLoggedDuration(_ sender: Any) {
        pasteboard.clearContents()
        pasteboard.setString(labelDetailsLoggedDuration.stringValue, forType: .string)
    }
    
    @IBAction func onCopyDateTime(_ sender: Any) {
        pasteboard.clearContents()
        pasteboard.setString(labelDetailsDateTimeCreated.stringValue, forType: .string)
    }
    
    @IBAction func onCopyDescription(_ sender: Any) {
        pasteboard.clearContents()
        pasteboard.setString(labelDetailsDescription.string, forType: .string)
    }
    
    @IBAction func onButtonCloseDetails(_ sender: Any) {
        statusbarSplitView.removeArrangedSubview(detailsViewItem)
        statusbarSplitView.addArrangedSubview(tableViewItem)
    }
    
    
    @IBAction func onMenuButton(_ sender: Any) {
        appdelegate.closePopover(sender: sender)
    }
    
    
    @IBAction func onButtonMore(_ sender: Any) {
        let lastTableCellView = statusbarTableView.view(atColumn: 0, row: lastTableViewSelection, makeIfNecessary: false)
        if let view = lastTableCellView as? StatusbarTableCellView {
            view.moreButton.isHidden = true
        }
        statusbarSplitView.addArrangedSubview(detailsViewItem)
        if sidebarOutlineView.item(atRow: sidebarOutlineView.selectedRow) is Release {
            let ticket = tickets[statusbarTableView.selectedRow]
            fillDetailsView(ticket: ticket)
        } else if sidebarOutlineView.item(atRow: sidebarOutlineView.selectedRow) is Ticket {
            let time = times[statusbarTableView.selectedRow]
            fillDetailsView(time: time)
        }
        statusbarTableView.deselectAll(self)
        statusbarSplitView.removeArrangedSubview(tableViewItem)
        self.view.layer?.layoutIfNeeded()
    }
    
    static func freshController() -> StatusbarViewController {
        let storyboard = NSStoryboard(name: "Main", bundle: nil)
        let identifier = "StatusbarController"
        let viewcontroller = storyboard.instantiateController(withIdentifier: identifier) as! StatusbarViewController
        return viewcontroller
    }
    
    let pasteboard = NSPasteboard.general
    
    var initValues: InitValues! = nil
    
    var tickets: [Ticket] = []
    var releases: [Release] = []
    var times: [Time] = []
    
    var sidebarViewItem: NSView!
    var tableViewItem: NSView!
    var detailsViewItem: NSView!
    
    var lastTableViewSelection: Int!
    
    let appdelegate = NSApplication.shared.delegate as! AppDelegate
    var managedObjectContext: NSManagedObjectContext?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getAllItems()
        sidebarOutlineView.delegate = self
        sidebarOutlineView.dataSource = self
        sidebarOutlineView.reloadData()
        
        statusbarTableView.delegate = self
        statusbarTableView.dataSource = self
        statusbarTableView.reloadData()
        
        let backgroundColor = NSColor.controlBackgroundColor.cgColor
        let color = CALayer()
        color.backgroundColor = backgroundColor
        self.view.layer = color
        expandAllItems()
        
        sidebarViewItem = statusbarSplitView.arrangedSubviews[0]
        tableViewItem = statusbarSplitView.arrangedSubviews[1]
        detailsViewItem = statusbarSplitView.arrangedSubviews[2]

        statusbarSplitView.removeArrangedSubview(detailsViewItem)
        labelDetailsDescription.delegate = self
        managedObjectContext = appdelegate.persistentContainer.viewContext
        NotificationCenter.default.addObserver(forName: .reloadStatusbar, object: managedObjectContext, queue: OperationQueue.main, using: {(notification) in
            self.getAllItems()
            self.sidebarOutlineView.reloadData()
            self.statusbarTableView.reloadData()
            self.expandAllItems()
            print("NOTIFICATION CALLED")
        })
    }
    
    
    private func expandAllItems() {
        for release in self.releases {
            sidebarOutlineView.expandItem(release)
        }
        for ticket in self.tickets {
            sidebarOutlineView.expandItem(ticket)
        }
    }
    
    private func getAllItems() {
        releases.removeAll()
        tickets.removeAll()
        times.removeAll()
        initValues = InitValues.init()
        releases = initValues.releases
        tickets = initValues.tickets
        times = initValues.times
    }
    
    private func fillDetailsView(ticket: Ticket) {
        let loggedDuration = TimeConverter.sharedInstance.GetStringValueFromTime(time: Int(ticket.duration))
        labelDetailsTitle.stringValue = ticket.ticketname!
        labelDetailsDateTimeCreated.stringValue = "\(ticket.date!) \(ticket.time!)"
        labelDetailsLoggedDuration.stringValue = loggedDuration
        labelDetailsDescription.string = ticket.details!
        copyLoggedTimeButton.isHidden = false
    }
    
    private func fillDetailsView(time: Time) {
        let loggedDuration = TimeConverter.sharedInstance.GetStringValueFromTime(time: Int(time.duration))
        labelDetailsTitle.stringValue = loggedDuration
        labelDetailsDateTimeCreated.stringValue = "\(time.date!) \(time.created!)"
        labelDetailsLoggedDuration.stringValue = ""
        copyLoggedTimeButton.isHidden = true
        labelDetailsDescription.string = time.details!
    }
 

}
