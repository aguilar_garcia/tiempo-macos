//
//  AddTimeStatusbarController.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 31.01.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import Cocoa

class AddTimeStatusbarController: NSViewController, NSTextFieldDelegate {
    
    @IBOutlet weak var TextFieldTitle: NSTextField!
    
    @IBOutlet weak var selectTicketPopup: NSPopUpButton!
    
    @IBOutlet weak var selectReleasePopup: NSPopUpButton!
    
    @IBOutlet weak var textFieldDuration: NSTextField!
    
    @IBOutlet weak var textFieldDescription: NSTextField!

    @IBOutlet weak var labelRelease: NSTextField!
    
    @IBOutlet weak var footerView: NSView!
    
    var tickets: [Ticket] = []
    var releases: [Release] = []
    var selectedRelease: Release?
    
    
    
    @IBAction func onReleasePopup(_ sender: Any) {
        changeTicketPopupValues()
    }
    
    @IBAction func onButtonCancel(_ sender: Any) {
        closeWindow(sender)
    }
    
    @IBAction func onButtonOk(_ sender: Any) {
        if textFieldDuration.stringValue == "" {
            return
        }

        let duration = TimeConverter.sharedInstance.ConvertStringToDuration(input: self.textFieldDuration.stringValue)
        if (duration > 0) {
            _ = releases[self.selectReleasePopup.indexOfSelectedItem]
            let getTicket = tickets[self.selectTicketPopup.indexOfSelectedItem]
            let ticketName = getTicket.ticketname!
            let ticketID = getTicket.ticketId!
            print("selected Ticket: \(ticketName) ID: \(ticketID)")
            _ = TimeManager.sharedInstance.addTime(ticketId: ticketID, duration: Int32(duration), details: textFieldDescription.stringValue)
            NotificationCenter.default.post(name: .reloadSidebar, object: managedObjectContext)
            NotificationCenter.default.post(name: .reloadStatusbar, object: managedObjectContext)
            NotificationCenter.default.post(name: .reloadReleaseTable, object: managedObjectContext)
            NotificationCenter.default.post(name: .reloadTimeTable, object: managedObjectContext)
            closeWindow(sender)
        } else {
            let alert = NSAlert()
            alert.messageText = "\(textFieldDuration.stringValue) is not a possible duration"
            alert.informativeText = "eg. 2d 1h 20min"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            alert.addButton(withTitle: "Cancel")
            alert.beginSheetModal(for: self.view.window!, completionHandler: { (modalResponse) -> Void in
                if modalResponse == NSApplication.ModalResponse.alertSecondButtonReturn {
                    self.dismiss(sender)
                }
            })
            //alert.runModal()
        }
    }
    
    var managedObjectContext: NSManagedObjectContext?
    
    var mainWindow: MainWindowController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate = NSApplication.shared.delegate as! AppDelegate
        self.managedObjectContext = appDelegate.persistentContainer.viewContext
        mainWindow = MainWindowController()
        let releaseList = changeReleasePopupValues()
        for release in releaseList {
            self.releases.append(release)
            self.selectReleasePopup.addItem(withTitle: release.releaseName!)
        }
        changeTicketPopupValues()

        self.labelRelease.stringValue = UserDefaults.standard.string(forKey: "Category") ?? "Release"
        self.textFieldDuration.delegate = self
        let backgroundColor = NSColor.controlBackgroundColor.cgColor
        let color = CALayer()
        color.backgroundColor = backgroundColor
        self.view.layer = color
        // Do view setup here.
    }
    
    func changeReleasePopupValues() -> [Release]{
        self.releases.removeAll()
        self.selectReleasePopup.removeAllItems()
        let tickets = TicketManager.sharedInstance.getTickets()
        let releases = ReleaseManager.sharedInstance.getReleases()
        var releasesWithTickets: [Release] = []
        for ticket in tickets {
            releasesWithTickets += releases.filter() {
                return ($0 as Release).releaseId == ticket.releaseId
            }
        }
        return releasesWithTickets
    }
    
    func changeTicketPopupValues() {
        self.tickets.removeAll()
        self.selectTicketPopup.removeAllItems()
        let ticketList = TicketManager.sharedInstance.getTicketsFromRelease(release: releases[self.selectReleasePopup.indexOfSelectedItem])
        for ticket in ticketList {
            self.tickets.append(ticket)
            self.selectTicketPopup.addItem(withTitle: ticket.ticketname!)
        }
    }
    
    func controlTextDidChange(_ obj: Notification) {
        if let textField = obj.object as? NSTextField, self.textFieldDuration.identifier == textField.identifier {
            textFieldDuration.backgroundColor = NSColor.darkGray
            self.TextFieldTitle.stringValue = textField.stringValue
            print("Changed text = \(textField.stringValue)\n")
        }
    }
    
    func closeWindow(_ sender: Any) {
        if let window = self.view.window?.windowController {
            window.close()
        } else {
            self.dismiss(sender)
        }
    }
    
    
}

