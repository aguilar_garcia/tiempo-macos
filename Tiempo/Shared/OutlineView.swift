//
//  OutlineView.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 30.01.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import Cocoa

class OutlineView: NSObject, NSOutlineViewDataSource {
    
    var initValues: InitValues
    
    var releases: [Release] = []
    var tickets: [Ticket] = []
    var times: [Time] = []
    
    var outlineView: NSOutlineView
    
    init(_ outlineView: NSOutlineView) {
        self.outlineView = outlineView
        self.initValues = InitValues.init()
        
        self.releases = initValues.releases
        self.tickets = initValues.tickets
        self.times = initValues.times
        super.init()
        
        outlineView.dataSource = self
        outlineView.reloadData()
    }
    
    func outlineView(_ outlineView: NSOutlineView, numberOfChildrenOfItem item: Any?) -> Int {
        if let release = item as? Release {
            let releaseChilds = self.tickets.filter {
                return ($0 as Ticket).releaseId == release.releaseId
            }
            return releaseChilds.count
        }
        return releases.count
    }
    
    func outlineView(_ outlineView: NSOutlineView, isItemExpandable item: Any) -> Bool {
        if let release = item as? Release {
            let releaseChild = self.tickets.filter {
                return ($0 as Ticket).releaseId == release.releaseId
            }
            return releaseChild.count > 0
        }
        return false
    }
    
    func outlineView(_ outlineView: NSOutlineView, child index: Int, ofItem item: Any?) -> Any {
        if let release = item as? Release {
            let releaseChild = self.tickets.filter {
                return ($0 as Ticket).releaseId == release.releaseId
            }
            return releaseChild[index]
        }
        return releases[index]
    }
    
    
}
