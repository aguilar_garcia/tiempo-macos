//
//  InitiValues.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 02.02.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import Foundation
import Cocoa

class InitValues {
    
    var tickets: [Ticket] = []
    var releases: [Release] = []
    var times: [Time] = []
    
    init() {
        self.releases.removeAll()
        self.tickets.removeAll()
        self.times.removeAll()
        
        let dataRelease = ReleaseManager.sharedInstance.getReleases()
        let dataTicket = TicketManager.sharedInstance.getTickets()
        let dataTime = TimeManager.sharedInstance.getTimes()
        
        for release in dataRelease {
            self.releases.append(release)
        }
        
        for ticket in dataTicket {
            self.tickets.append(ticket)
        }
        
        for time in dataTime {
            self.times.append(time)
        }
    }
    
}
