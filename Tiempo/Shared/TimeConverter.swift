//
//  TimeConverter.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 12.03.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import Foundation

class TimeConverter {
    
    static let sharedInstance = TimeConverter()
    
    func GetStringValueFromTime(time: Int) -> String {
        var duration = ""
        let days = time / 60 / 8
        duration = days > 0 ? "\(days)d " : ""
        let hours = time / 60 - (days * 8)
        duration = hours > 0 ? duration + "\(hours)h " : duration + ""
        let minutes = time % 60
        duration = minutes > 0 ? duration + "\(minutes)m" : duration + ""
        duration = duration.trimmingCharacters(in: .whitespacesAndNewlines)
        return duration
    }
    
    public func ConvertStringToDuration(input: String) -> Int {
        let daysInMinutes = ConvertDaysToHours(inputField: input)
        let hoursInMinutes = ConvertHours(inputField: input)
        let minutesFromInput = ConvertMinutes(inputField: input)
        let duration = daysInMinutes + hoursInMinutes + minutesFromInput
        return duration
    }
    
    private func ConvertDaysToHours(inputField: String) -> Int {
        let input = inputField.trimmingCharacters(in: .whitespacesAndNewlines)
        if input.contains("d") {
            let charIndex = input.index(of: "d")
            let pref = input.prefix(upTo: charIndex!)
            if let days = Int(pref) {
                let hours = days * 8 * 60
                print(hours)
                return hours
            }
        }
        return 0
    }
    
    private func ConvertHours(inputField: String) -> Int {
        let input = inputField.trimmingCharacters(in: .whitespacesAndNewlines)
        if input.contains("h") && input.contains("d") {
            let startIndex = input.index(of: "d")
            let afterIndex = input.index(after: startIndex!)
            let endIndex = input.index(of: "h")
            let subOne = input.prefix(upTo: endIndex!)
            let hours = subOne.suffix(from: afterIndex).trimmingCharacters(in: .whitespacesAndNewlines)
            if let hours = Int(hours) {
                let minutes = hours * 60
                return minutes
            }
        } else if input.contains("h") {
            let endIndex = input.index(of: "h")
            let hours = input.prefix(upTo: endIndex!).trimmingCharacters(in: .whitespacesAndNewlines)
            if let hours = Int(hours) {
                let minutes = hours * 60
                return minutes
            }
        }
        return 0
    }
    
    private func ConvertMinutes(inputField: String) -> Int {
        let input = inputField.trimmingCharacters(in: .whitespacesAndNewlines)
        if input.contains("h") && input.contains("m") {
            let startIndex = input.index(of: "h")
            let afterIndex = input.index(after: startIndex!)
            let endIndex = input.index(of: "m")
            let subOne = input.prefix(upTo: endIndex!)
            let minutes = subOne.suffix(from: afterIndex).trimmingCharacters(in: .whitespacesAndNewlines)
            if let minutes = Int(minutes) {
                return minutes
            }
        } else if input.contains("m") {
            let endIndex = input.index(of: "m")
            let hours = input.prefix(upTo: endIndex!).trimmingCharacters(in: .whitespacesAndNewlines)
            if let minutes = Int(hours) {
                return minutes
            }
        }
        return 0
    }
    
}
