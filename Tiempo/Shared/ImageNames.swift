//
//  ImageNames.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 04.03.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import Cocoa

extension NSImage.Name {
    static let project = NSImage.Name("Project")
    static let ticket = NSImage.Name("Ticket")
    static let time = NSImage.Name("Time")
    static let alarmOn = NSImage.Name("Alarm-filled")
    static let alarmOff = NSImage.Name("Alarm")
}
