//
//  Footer.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 10.03.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import Cocoa

class Footer: NSView {

    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        NSColor(red: 0/255, green: 40/255, blue: 101/255, alpha: 1.0).setFill()
        dirtyRect.fill()
    }
    
}
