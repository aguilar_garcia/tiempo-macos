//
//  NotificationNames.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 10.02.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import Foundation

extension Notification.Name {
    
    static let reloadSidebar = Notification.Name(rawValue: "reloadSidebar")
    static let reloadStatusbar = Notification.Name(rawValue: "reloadStatusbar")
    static let reloadReleaseTable = Notification.Name(rawValue: "reloadReleaseTable")
    static let mainCategoryNameChanged = Notification.Name(rawValue: "mainCategoryChanged")
    static let reloadTimeTable = Notification.Name(rawValue: "reloadTimeTable")
    static let itemChanged = Notification.Name(rawValue: "checkForAddButtons")

}
