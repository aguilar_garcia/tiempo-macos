//
//  TimeCell.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 10.03.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import Cocoa

class TimeCell: NSTableCellView {

    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        // Drawing code here.
    }
    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        
        setUpTrackingArea()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setUpTrackingArea()
    }
    
    func setUpTrackingArea() {
        let trackingArea = NSTrackingArea(rect: self.frame, options: NSTrackingArea.Options.mouseEnteredAndExited, owner: self, userInfo: nil)
        self.addTrackingArea(trackingArea)
    }

    override func mouseEntered(with event: NSEvent) {
        print("mouse entered")
    }
    
    override func mouseExited(with event: NSEvent) {
        print("mouse exited")
    }

}
