//
//  SettingsWindowController.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 03.02.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import Cocoa

class SettingsWindowController: NSWindowController {

    override func windowDidLoad() {
        super.windowDidLoad()
    
        self.window?.center()
        windowTitle(forDocumentDisplayName: "Settings")
        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    }   

}
