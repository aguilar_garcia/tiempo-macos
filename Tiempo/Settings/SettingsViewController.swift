//
//  SettingsViewController.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 03.02.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import Cocoa
import UserNotifications
import Sparkle

class SettingsViewController: NSViewController, NSTextFieldDelegate {

    @IBAction func onAllowNotificationsButton(_ sender: Any) {
        changeNotificationSettings()
    }
    
    @IBOutlet weak var onAllowNotificationOutlet: NSButton!
    
    @IBOutlet weak var textFieldNotificationDuration: NSTextField!
    
    @IBOutlet weak var textFieldMainCategoryName: NSTextField!
    
    @IBAction func onCloseButton(_ sender: Any) {
        closeWindow(sender)
    }
    
    @IBAction func onAllowAutoUpdate(_ sender: Any) {
    }
    
    @IBAction func onCheckForUpdateNow(_ sender: Any) {
    }
    
    @IBAction func onTextFieldNotificationDuration(_ sender: Any) {
    }
    
    @IBOutlet weak var lblVersion: NSTextField!
    
    let appDelegate = NSApplication.shared.delegate as! AppDelegate
    var managedObjectContext: NSManagedObjectContext?

    let defaults = UserDefaults.standard
    
    func changeNotificationSettings() {
        if (onAllowNotificationOutlet.state == NSControl.StateValue.on) {
            var inputDuration = textFieldNotificationDuration.stringValue
            if inputDuration.isEmpty || inputDuration.starts(with: "0"){
                textFieldNotificationDuration.stringValue = "15"
                inputDuration = "15"
            }
            if let intDuration = Int32(inputDuration) {
                let timeInterval = Double(intDuration)
                defaults.set(timeInterval, forKey: "NotificationDuration")
                defaults.set(true, forKey: "isNotificationsEnabled")
                print("NotificationDuration set to: ", intDuration)
            } else {
                defaults.set(true, forKey: "isNotificationsEnabled")
                textFieldNotificationDuration.stringValue = ""
                defaults.set(0, forKey: "NotificationDuration")
            }
            appDelegate.unSetNotifications()
            appDelegate.setNotificationSettings()
        } else {
            textFieldNotificationDuration.stringValue = ""
            defaults.set(false, forKey: "isNotificationsEnabled")
            appDelegate.unSetNotifications()
            print("onAllowNotification State off: \(onAllowNotificationOutlet.state)")
        }
    }
    
    private func setVersion() {
        guard let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String else { return }
        guard let application = Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as? String else { return }
        print("Version = \(String(describing: version))")
        self.lblVersion.stringValue = "\(application) \(version)"
    }
    
    let notificationCenter = UNUserNotificationCenter.current()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldNotificationDuration.delegate = self
        textFieldNotificationDuration.stringValue = "\(defaults.integer(forKey: "NotificationDuration"))"
        textFieldMainCategoryName.stringValue = defaults.string(forKey: "Category")!
        self.managedObjectContext = appDelegate.persistentContainer.viewContext
        let isNotificationEnabled = defaults.bool(forKey: "isNotificationsEnabled")
        if isNotificationEnabled {
            onAllowNotificationOutlet.state = .on
        } else {
            onAllowNotificationOutlet.state = .off
        }
        setVersion()
        let backgroundColor = NSColor.controlBackgroundColor.cgColor
        let color = CALayer()
        color.backgroundColor = backgroundColor
        self.view.layer = color
        // Do view setup here.
    }
    
    
    // Observe text change 'textFieldNotificationDuration'
    func controlTextDidChange(_ obj: Notification) {
        guard let currentTextFieldInput = obj.object as? NSTextField else {
            return
        }

        let textFieldInput = currentTextFieldInput.stringValue
        if let _ = Int32(textFieldInput) {
            appDelegate.unSetNotifications()
            onAllowNotificationOutlet.state = .on
        } else if textFieldInput.isEmpty || textFieldInput.starts(with: "0") {
            onAllowNotificationOutlet.state = .off
        } else {
            textFieldNotificationDuration.stringValue = String(textFieldInput.dropLast())
        }
        changeNotificationSettings()
    }
    
    override func viewDidDisappear() {
        let mainCategoryTextFieldInput = textFieldMainCategoryName.stringValue
        defaults.set(mainCategoryTextFieldInput, forKey: "Category")
        NotificationCenter.default.post(name: .mainCategoryNameChanged, object: managedObjectContext)
    }
    
    func closeWindow(_ sender: Any) {
        if let window = self.view.window?.windowController {
            window.close()
        } else {
            self.dismiss(sender)
        }
    }
    
    
}
