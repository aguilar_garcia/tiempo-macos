//
//  AppDelegate.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 26.01.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import Cocoa
import Sparkle
import UserNotifications

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate, NSPopoverDelegate, UNUserNotificationCenterDelegate {
   
    let statusItem = NSStatusBar.system.statusItem(withLength: NSStatusItem.squareLength)
    let popOver = NSPopover()
    var popOverMonitor: Any?
    let defaults = UserDefaults.standard
    let notificationCenter = UNUserNotificationCenter.current()
    
    // setup notifitcations
    func setNotificationSettings() {
        notificationCenter.getNotificationSettings( completionHandler: {(notificationSettings) in
            if notificationSettings.authorizationStatus == .authorized {
                self.addNotifications()
            } else if notificationSettings.authorizationStatus == .notDetermined {
                self.requestAuthorization(completionHandler: { (success) in
                    guard success else { return }
                    self.addNotifications()
                })
            } else if notificationSettings.authorizationStatus == .provisional {
                self.addNotifications()
            } else if notificationSettings.authorizationStatus == .denied {
                self.unSetNotifications()
                self.requestAuthorization(completionHandler: {(success) in
                    guard success else { return }
                    self.addNotifications()
                })
            }
        })
    }
    
    private func addNotifications() {
        let notificationDuration = defaults.double(forKey: "NotificationDuration") * 60
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: notificationDuration, repeats: true)
        let request = createNotificationRequest(trigger)
        
        notificationCenter.add(request, withCompletionHandler: {(error) in
            if error != nil {
                print("Error: ", error as Any)
        }})
    }
    
    
    private func requestAuthorization(completionHandler: @escaping (_ success: Bool) -> ()) {
        // Request Authorization
        notificationCenter.requestAuthorization(options: [.alert, .sound, .badge]) { (success, error) in
            if let error = error {
                print("Request Authorization Failed (\(error), \(error.localizedDescription))")
            }
            completionHandler(success)
        }
    }
    
    // removes all notifications
    func unSetNotifications() {
        notificationCenter.removeAllPendingNotificationRequests()
        notificationCenter.removeAllDeliveredNotifications()
    }
    

    // creates a specific notification request with title and body
    private func createNotificationRequest(_ trigger: UNTimeIntervalNotificationTrigger) -> UNNotificationRequest {
        let uuidString = UUID().uuidString
        let content = UNMutableNotificationContent()
        content.title = "Time reminder"
        content.body = "Book your Time!"
        content.categoryIdentifier = "alarm"
        
        let request = UNNotificationRequest(identifier: uuidString, content: content, trigger: trigger)
        return request
    }
    
    
    @objc func togglePopOver(_ sender: Any?) {
        popOver.isShown ? closePopover(sender: sender) : openPopover(sender: sender)
    }
    
    
    func openPopover(sender: Any?) {
        if let statusButton = statusItem.button {
            popOver.show(relativeTo: NSZeroRect, of: statusButton, preferredEdge: NSRectEdge.minY)
            statusButton.highlight(true)
            popOverMonitor = NSEvent.addGlobalMonitorForEvents(matching: NSEvent.EventTypeMask.leftMouseDown, handler: { (event: NSEvent!) -> Void in
                self.closePopover(sender: sender)
            })
        }
    }
    
    
    func closePopover(sender: Any?) {
        if let statusButton = statusItem.button {
            popOver.performClose(sender)
            statusButton.highlight(false)
        }
        if let monitor : Any = popOverMonitor {
            NSEvent.removeMonitor(monitor)
            popOverMonitor = nil
        }
    }
    
    
    private func setStandardUserDefaults() {
        UserDefaults.standard.register(defaults: [
            "Category" : "Project",
            "isNotificationsEnabled": false,
            "NotificationDuration": 15
            ])
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        //let userInfo = response.notification.request.content.userInfo
        if response.notification.request.content.categoryIdentifier == "alarm" {
            switch response.actionIdentifier {
            case UNNotificationDefaultActionIdentifier:
                // the user swiped to unlock
                print("Default identifier")
            case "close":
                break
            case "open":
                break;
            default:
                break
            }
        }
    }
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        if let statusbarButton = statusItem.button {
            statusbarButton.image = NSImage(named: "StatusbarIcon")
            statusbarButton.action = #selector(togglePopOver(_:))
        }
        popOver.contentViewController = StatusbarViewController.freshController()
        popOver.delegate = nil
        notificationCenter.delegate = self
        let show = UNNotificationAction(identifier: "close", title: "close", options: .destructive)
        let close = UNNotificationAction(identifier: "open", title: "open", options: .foreground)
        let category = UNNotificationCategory(identifier: "alarm", actions: [show, close], intentIdentifiers: [])
        notificationCenter.setNotificationCategories([category])
        setStandardUserDefaults()
        if defaults.bool(forKey: "isNotificationsEnabled") {
            addNotifications()
        }
        
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
    
    
    func applicationShouldHandleReopen(_ sender: NSApplication, hasVisibleWindows flag: Bool) -> Bool {
        if !flag {
            for window in sender.windows {
                window.makeKeyAndOrderFront(self)
            }
        }
        return true
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Tiempo")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

