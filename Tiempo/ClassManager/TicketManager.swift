//
//  TicketManager.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 30.01.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import AppKit
import Cocoa
import CoreData

class TicketManager: NSObject {
    static let sharedInstance = TicketManager()
    
    let ticketEntityName = "Ticket"
    let context: NSManagedObjectContext
    private var ticketContext = 0
    var tickets: [Ticket] = []
    
    override init() {
        let appDelegate = NSApplication.shared.delegate as! AppDelegate
        self.context = appDelegate.persistentContainer.viewContext
        super.init()
        self.tickets = getTickets()
    }

    
    func addTicket(releaseId: String?, ticketname: String, details: String, isImportant: Bool) -> Ticket? {
        guard let newEntity = NSEntityDescription.entity(forEntityName: ticketEntityName, in: context) else { return nil }
        
        let newTicket = Ticket(entity: newEntity, insertInto: self.context)
        let newTicketId = UUID().uuidString
        
        newTicket.releaseId = releaseId
        newTicket.ticketname = ticketname
        newTicket.details = details
        newTicket.ticketId = newTicketId
        newTicket.time = getCurrentTime()
        newTicket.date = getCurrentDate()
        newTicket.isImportant = isImportant
        print(newTicket)
        do {
            try context.save()
            self.tickets = getTickets()
            return newTicket
        } catch {
            return nil
        }
    }
    
    func updateTicket(ticket: Ticket, value: Any, forKey: String) {
        ticket.setValue(value, forKey: forKey)
        do {
            try context.save()
        } catch {
            print("Update throws : \(error)")
        }
    }
    
    func deleteTicket(ticket: Ticket) {
        let filterdTimes = TimeManager.sharedInstance.getTimes().filter() {
            return ($0 as Time).ticketId == ticket.ticketId
        }
        for time in filterdTimes {
            TimeManager.sharedInstance.deleteTime(time: time)
        }
        context.delete(ticket)
        do {
            try context.save()
        } catch {
            print(error)
        }
        self.tickets = getTickets()
    }
    
    
    func deleteAllTickets() {
        let ticketList = getTickets()
        for ticket in ticketList {
            deleteTicket(ticket: ticket)
        }
        self.tickets = getTickets()
    }
    
    func countTickets() -> Int {
        let ticketsCounter = getTickets()
        return ticketsCounter.count
    }
    
    
    func getTickets() -> [Ticket] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: ticketEntityName)
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request) as! [Ticket]
            tickets = result
            return result
        } catch {
            print(error)
            return tickets
        }
    }
    
    func getTicketsFromRelease(release: Release) -> [Ticket] {
        let ticketList = tickets.filter {
            return ($0 as Ticket).releaseId == release.releaseId
        }
        return ticketList
    }
    
    
    func getEstimatedTime(_ ticket: Ticket) -> Int32 {
        let times = TimeManager.sharedInstance.getTimes()
        let timeOfTicket = times.filter {
            return ($0 as Time).ticketId == ticket.ticketId
        }
        var completeTimeEstimated: Int32 = 0
        for estimated in timeOfTicket {
            completeTimeEstimated = completeTimeEstimated + estimated.duration
        }
        ticket.duration = completeTimeEstimated
        return completeTimeEstimated
    }
    
    private func getCurrentTime() -> String {
        let date = Date()
        let timeFormatter = DateFormatter()
        timeFormatter.locale = Locale.current
        timeFormatter.timeStyle = .short
        timeFormatter.dateFormat = "HH:mm"
        let currentTime = timeFormatter.string(from: date)
        
        return currentTime
    }

    
    private func getCurrentDate() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateStyle = .short
        formatter.dateFormat = "dd.MM.yyyy"
        let stringDate = formatter.string(from: date)
        return stringDate
    }

    
}

