//
//  TimeManager.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 31.01.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import Cocoa

class TimeManager {
    
    static let sharedInstance = TimeManager()
    
    let timeEntityName = "Time"
    let context: NSManagedObjectContext
    var times: [Time] = []
    
    init() {
        let appDelegate = NSApplication.shared.delegate as! AppDelegate
        self.context = appDelegate.persistentContainer.viewContext
        self.times = getTimes()
    }
    
    
    func addTime(ticketId: String?, duration: Int32, details: String) -> Time? {
        guard let newEntity = NSEntityDescription.entity(forEntityName: timeEntityName, in: context) else { return nil }
        
        let newTime = Time(entity: newEntity, insertInto: self.context)
        
        newTime.details = details
        newTime.duration = duration
        newTime.ticketId = ticketId
        newTime.date = getCurrentDate()
        newTime.created = getCurrentTime()
        var formatter = DateFormatter()
        formatter.dateStyle = .short
        let date = NSDate()
        let calender = NSCalendar.current
        
        
        do {
            try context.save()
            self.times = getTimes()
            return newTime
        } catch {
            return nil
        }
    }
    
    func updateTime(time: Time, value: Any, forKey: String) {
        time.setValue(value, forKey: forKey)
        do {
            try context.save()
        } catch {
            print("Update time throws: \(error)")
        }
    }
    
    
    func deleteTime(time: Time) {
        context.delete(time)
        do {
            try context.save()
        } catch {
            print(error)
        }
        self.times = getTimes()
    }
    
    
    func deleteAllTimes() {
        let timesList = getTimes()
        for time in timesList {
            deleteTime(time: time)
        }
        self.times = getTimes()
    }
    
    
    func getTimes() -> [Time] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: timeEntityName)
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request) as! [Time]
            self.times = result
            return result
        } catch {
            print(error)
            return self.times
        }
    }
    
    
    func getTimesFromTicket(ticket: Ticket) -> [Time] {
        let timeOfTicket = times.filter {
            return ($0 as Time).ticketId == ticket.ticketId
        }
        return timeOfTicket
    }
    
    
    private func getCurrentTime() -> String {
        let date = Date()
        let timeFormatter = DateFormatter()
        timeFormatter.locale = Locale.current
        timeFormatter.timeStyle = .short
        timeFormatter.dateFormat = "HH:mm"
        let currentTime = timeFormatter.string(from: date)
        
        return currentTime
    }
    
    
    private func getCurrentDate() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateStyle = .short
        formatter.dateFormat = "dd.MM.yyyy"
        let stringDate = formatter.string(from: date)
        return stringDate
    }
    
    func sortBy(value: String) {
        let request = NSFetchRequest<Time>(entityName: timeEntityName)
        let sort = NSSortDescriptor(key: "duration", ascending: true)
        request.sortDescriptors = [sort]
    }
    
}
