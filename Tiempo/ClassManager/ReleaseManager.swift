//
//  ReleaseManager.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 31.01.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import CoreData
import Cocoa

class ReleaseManager {
    
    static let sharedInstance = ReleaseManager()
    
    let releaseEntityName = "Release"
    let context: NSManagedObjectContext
    var releases: [Release] = []
    
    init() {
        let appDelegate = NSApplication.shared.delegate as! AppDelegate
        self.context = appDelegate.persistentContainer.viewContext
        self.releases = getReleases()
    }
    
    
    func addRelease(releaseName: String?) -> Release? {
        guard let newEntity = NSEntityDescription.entity(forEntityName: releaseEntityName, in: self.context) else { return nil }
        
        let newRelease = Release(entity: newEntity, insertInto: self.context)
        let newReleaseId = UUID().uuidString
        
        newRelease.releaseName = releaseName
        newRelease.releaseId = newReleaseId
        
        do {
            try context.save()
            self.releases = getReleases()
            print(newRelease)
            return newRelease
        } catch {
            return nil
        }
    }
    
    
    func deleteRelease(release: Release) {
        let filteredTickets = TicketManager.sharedInstance.getTickets().filter() {
            return ($0 as Ticket).releaseId == release.releaseId
        }
        for ticket in filteredTickets {
            TicketManager.sharedInstance.deleteTicket(ticket: ticket)
        }
        context.delete(release)
        do {
            try context.save()
        } catch {
            print(error)
        }
        self.releases = getReleases()
    }
    
    
    func deleteAllReleases() {
        let releaseList = getReleases()
        for release in releaseList {
            deleteRelease(release: release)
        }
        self.releases = getReleases()
    }
    
    func getEstimatedTime(_ release: Release) -> Int32 {
        let tickets = TicketManager.sharedInstance.getTickets()
        let ticketsInRelease = tickets.filter {
            return ($0 as Ticket).releaseId == release.releaseId
        }
        
        var completeTimeEstimated: Int32 = 0
        for estimated in ticketsInRelease {
            let time = TicketManager.sharedInstance.getEstimatedTime(estimated)
            completeTimeEstimated = completeTimeEstimated + time
        }
        return completeTimeEstimated
    }
    
    func getReleases() -> [Release] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: releaseEntityName)
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request) as! [Release]
            self.releases = result
            return result
        } catch {
            print(error)
            return self.releases
        }
    }
    
    
}
