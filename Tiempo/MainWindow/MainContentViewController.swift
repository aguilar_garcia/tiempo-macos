//
//  MainContentViewController.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 02.02.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import Cocoa

class MainContentViewController: NSViewController, NSTextViewDelegate {

    static let sharedInstance = MainContentViewController()
    
    
    @IBOutlet weak var timeScrollView: NSScrollView!
    
    @IBOutlet weak var imageButtonTicket: NSButton!
    
    @IBOutlet weak var importantButton: NSButton!
    
    @IBOutlet weak var timeTextField: NSTextField!
    
    @IBOutlet var detailsTextView: NSTextView!
    
    @IBOutlet weak var buttonCopyDesc: NSButton!
    
    @IBOutlet weak var buttonEditDesc: NSButton!
    
    @IBOutlet weak var buttonCopyTicketName: NSButton!
    
    @IBAction func onImageViewCopyTimeDuration(_ sender: Any) {
        let pasteboard = NSPasteboard.general
        pasteboard.clearContents()
        
        pasteboard.setString(ticketTitleLabel.stringValue, forType: .string)
    }
    
    
    @IBAction func onTicketTitle(_ sender: Any) {
        fillMainContent(ticket: self.ticket!)
        timeTableView.deselectAll(sender)
    }
    @IBOutlet weak var ticketTitleLabel: NSButton!
    
    @IBOutlet weak var boxDescription: NSBox!
    
    @IBOutlet weak var timeTableView: NSTableView!
    
    @IBAction func onButtonImportant(_ sender: Any) {
        importantButton.image = importantButton.state == .on ? NSImage(named: .alarmOff) : NSImage(named: .alarmOn)
        mainWindow?.selectedTicket?.isImportant = importantButton.state == .on
    }
    
    @IBAction func onButtonTicketTitle(_ sender: Any) {
        fillMainContent(ticket: self.ticket!)
    }
    
    @IBAction func onButtonCopyTcket(_ sender: Any) {
        let pasteboard = NSPasteboard.general
        pasteboard.clearContents()
        pasteboard.setString(ticket!.ticketname!, forType: .string)
    }
    
    @IBAction func onButtonCopyDesc(_ sender: Any) {
        let pasteboard = NSPasteboard.general
        pasteboard.clearContents()
        pasteboard.setString(detailsTextView.string, forType: .string)
    }
    
    @IBAction func onButtonEditDesc(_ sender: Any) {
        detailsTextView.isEditable = !detailsTextView.isEditable
        buttonEditDesc.isHighlighted = !buttonEditDesc.isHighlighted
    }
    
    let appDelegate = NSApplication.shared.delegate as! AppDelegate
    var managedObjectContext: NSManagedObjectContext?
    var mainWindow: MainWindowController? = nil
    var ticket: Ticket? = nil
    var time: Time? = nil
    var times: [Time] = []
    var cellView: NSTableCellView?
    let pasteboard = NSPasteboard.general
    
    override func viewDidAppear() {
        self.mainWindow = self.view.window?.windowController as? MainWindowController
        self.mainWindow!.mainContentViewController = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.clearMainContent()
        self.detailsTextView.textColor = NSColor.textColor
        buttonEditDesc.contentTintColor = NSColor.textColor
        buttonCopyDesc.contentTintColor = NSColor.textColor
        managedObjectContext = appDelegate.persistentContainer.viewContext
        
        boxDescription.isHidden = true
        ticketTitleLabel.isHidden = true
        timeScrollView.isHidden = true
        buttonCopyTicketName.isHidden = true
        importantButton.isHidden = true
        imageButtonTicket.isHidden = true
        NotificationCenter.default.addObserver(forName: .reloadTimeTable, object: managedObjectContext, queue: OperationQueue.main, using: {(notification) in
            self.times.removeAll()
            let selectedTicket = self.mainWindow?.selectedTicket
            self.ticket = selectedTicket
            if selectedTicket != nil {
                let filteredTimes = TimeManager.sharedInstance.getTimesFromTicket(ticket: selectedTicket!)
                for time in filteredTimes {
                    self.times.append(time)
                }
                
            }
            //self.importantButton.image = (selectedTicket?.isImportant)! ? NSImage(named: .alarmOn) : NSImage(named: .alarmOff)
            self.ticketTitleLabel.title = selectedTicket?.ticketname ?? ""
            self.timeTableView.reloadData()
            self.boxDescription.isHidden = false
            self.ticketTitleLabel.isHidden = false
            self.timeScrollView.isHidden = false
            self.buttonCopyTicketName.isHidden = false
            self.imageButtonTicket.isHidden = false
            print("TicketNotification CALLED")
        })

        detailsTextView.delegate = self
        timeTableView.dataSource = self
        timeTableView.delegate = self
        timeTableView.reloadData()

    }
    
    func textDidEndEditing(_ notification: Notification) {
        guard let _ = notification.object as? NSTextView else { return }
        detailsTextView.isEditable = false
        let selectedTimeTableRow = timeTableView.selectedRow
        let editedText = detailsTextView.string
        if selectedTimeTableRow != -1 {
            let time = times[selectedTimeTableRow]
            TimeManager.sharedInstance.updateTime(time: time, value: editedText, forKey: "details")
        } else {
            TicketManager.sharedInstance.updateTicket(ticket: mainWindow!.selectedTicket!, value: editedText, forKey: "details")
        }
    }
    
    func clearMainContent() {
        if self.time != nil  {
            self.detailsTextView.string = self.time!.details!
            self.timeTextField.stringValue = "\(self.time!.duration)"
        } else if self.ticket != nil {
            self.detailsTextView.string = self.ticket!.details!
            self.timeTextField.stringValue = "\(self.ticket!.duration)"
        } else {
            self.detailsTextView.string = ""
            self.timeTextField.stringValue = ""
        }
    }
    
    @objc func setMainDescription() {
        detailsTextView?.string = self.ticket?.details ?? ""
    }
    
    func fillMainContent(ticket: Ticket) {
        let estimatedTime = TicketManager.sharedInstance.getEstimatedTime(ticket)
        detailsTextView?.string = ticket.details ?? ""
        timeTextField?.stringValue = TimeConverter.sharedInstance.GetStringValueFromTime(time: Int(estimatedTime))
    }
    
    
    func fillMainContent(time: Time?) {
        self.detailsTextView?.string = time?.details ?? ""
        self.timeTextField?.stringValue = TimeConverter.sharedInstance.GetStringValueFromTime(time: Int(time!.duration))
    }
    
}
