//
//  SidebarViewController.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 02.02.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import Cocoa

class SidebarViewController: NSViewController {
    
    static let sharedInstance = SidebarViewController()
    
    @IBOutlet weak var outlineView: NSOutlineView!
    
    var initValues: InitValues!
    var sharedOutlineView: OutlineView!
    var mainWindow: MainWindowController!
    var splitViewController: MainSplitViewController!
    
    var contentItem: NSSplitViewItem!
    var releaseItem: NSSplitViewItem!
    
    let appDelegate = NSApplication.shared.delegate as! AppDelegate
    let pasteboard = NSPasteboard.general
    var releases: [Release] = []
    var tickets: [Ticket] = []
    var times: [Time] = []
    
    var managedObjectContext: NSManagedObjectContext?
    
    override func viewDidLoad() {
        managedObjectContext = appDelegate.persistentContainer.viewContext
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(forName: .reloadSidebar, object: managedObjectContext, queue: OperationQueue.main, using: {(notification) in
            self.sharedOutlineView = nil
            self.sharedOutlineView = OutlineView(self.outlineView)
            self.getAllItems()
            self.outlineView.reloadData()
            self.expandAllItems()
        })
        
        splitViewController?.removeSplitViewItem(self.releaseItem)
        let colorBottom = NSColor(red: 1 / 255, green: 58 / 255, blue: 144 / 255, alpha: 1).cgColor
        let colorTop = NSColor(red: 0 / 255, green: 40 / 255, blue: 101 / 255, alpha: 1).cgColor
        let gradient  = CAGradientLayer()
        gradient.colors = [ colorTop, colorBottom]
        gradient.locations = [ 0.0, 1.0]
        self.view.layer?.borderColor = CGColor.clear
        self.view.layer?.borderWidth = 0
        // Do view setup here.
    }
    
    
    override func viewDidAppear() {
        sharedOutlineView = OutlineView(outlineView)
        outlineView.delegate = self
        self.mainWindow = self.view.window?.windowController as? MainWindowController
        self.mainWindow!.mainSidebarViewController = self
        splitViewController = mainWindow!.mainSplitViewController
        
        contentItem = splitViewController?.splitViewItems[1]
        releaseItem = splitViewController?.splitViewItems[2]
        getAllItems()
        expandAllItems()
        outlineView.reloadData()
    }
    
    private func expandAllItems() {
        for release in self.releases {
            self.outlineView.expandItem(release)
        }
    }
    
    override func rightMouseDown(with event: NSEvent) {
        let menu = NSMenu()
        let category = UserDefaults.standard.string(forKey: "Category") ?? "Release"
        if ((outlineView.item(atRow: outlineView.selectedRow) as? Release) != nil) {
            menu.addItem(withTitle: "Copy \(category)name", action: #selector(onCopyReleaseText), keyEquivalent: "c")
            NSMenu.popUpContextMenu(menu, with: event, for: self.outlineView)
        } else if ((outlineView.item(atRow: outlineView.selectedRow) as? Ticket) != nil) {
            menu.addItem(withTitle: "Copy Ticketname", action: #selector(onCopyTicketname), keyEquivalent: "c")
            NSMenu.popUpContextMenu(menu, with: event, for: self.outlineView)
        }
    }
    
    @objc func onCopyReleaseText() {
        pasteboard.clearContents()
        let release = outlineView.item(atRow: outlineView.selectedRow) as! Release
        pasteboard.setString(release.releaseName!, forType: .string)
    }
    
    @objc func onCopyTicketname() {
        pasteboard.clearContents()
        let ticket = outlineView.item(atRow: outlineView.selectedRow) as! Ticket
        pasteboard.setString(ticket.ticketname!, forType: .string)

    }
    
    private func getAllItems() {
        releases.removeAll()
        tickets.removeAll()
        times.removeAll()
        releases = sharedOutlineView.releases
        tickets = sharedOutlineView.tickets
        times = sharedOutlineView.times
    }
    
}
