//
//  SidebarViewOutlineDelegate.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 17.03.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import Cocoa

extension SidebarViewController: NSOutlineViewDelegate {
    
    func outlineView(_ outlineView: NSOutlineView, shouldShowCellExpansionFor tableColumn: NSTableColumn?, item: Any) -> Bool {
        return false
    }
    
    func outlineView(_ outlineView: NSOutlineView, shouldShowOutlineCellForItem item: Any) -> Bool {
        return false
    }
    
    func outlineView(_ outlineView: NSOutlineView, viewFor tableColumn: NSTableColumn?, item: Any) -> NSView? {
        var view: NSTableCellView?
        
        if let release = item as? Release {
            view = outlineView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "HeaderCell"), owner: self) as? NSTableCellView
            if let textField = view?.textField {
                textField.stringValue = release.releaseName ?? ""
            }
            if let imageView = view?.imageView {
                imageView.image = NSImage.init(named:.project)
            }
        } else if let ticket = item as? Ticket {
            view = outlineView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "DataCell"), owner: self) as? NSTableCellView
            if let textfield = view?.textField {
                textfield.stringValue = ticket.ticketname ?? ""
            }
            if let imageView = view?.imageView {
                imageView.image = NSImage.init(named:.ticket)
            }
        } else {
            print("OutlineView: Unknown type item \(item)")
        }
        view?.rowSizeStyle = NSTableView.RowSizeStyle.large
        return view
    }
    
    func outlineView(_ outlineView: NSOutlineView, heightOfRowByItem item: Any) -> CGFloat {
        return 25;
    }
    
    func outlineViewSelectionDidChange(_ notification: Notification) {
        guard let outlineView = notification.object as? NSOutlineView else {
            return
        }
        let selectedIndex = outlineView.selectedRow
        if let item = self.outlineView.item(atRow: selectedIndex) as? Ticket {
            print("Ticket clicked in Sidebar")
            if splitViewController?.splitViewItem(for: mainWindow!.releaseContentViewController!) != nil {
                splitViewController?.removeSplitViewItem(self.releaseItem)
                splitViewController?.addSplitViewItem(self.contentItem)
                print("releasecontent is nil")
            }
            mainWindow!.mainContentViewController?.fillMainContent(ticket: item)
            mainWindow!.selectedTicket = item
            NotificationCenter.default.post(name: .reloadTimeTable, object: managedObjectContext)
        } else if let release = self.outlineView.item(atRow: selectedIndex) as? Release {
            print("Release cicked")
            if splitViewController?.splitViewItem(for: mainWindow!.mainContentViewController!) != nil {
                splitViewController?.removeSplitViewItem(self.contentItem)
                splitViewController?.addSplitViewItem(self.releaseItem)
            }
            mainWindow!.selectedRelease = release
            NotificationCenter.default.post(name: .reloadReleaseTable, object: managedObjectContext)
        }
    }
    
    
}
