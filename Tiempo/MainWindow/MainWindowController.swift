//
//  MainWindowController.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 31.01.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import Cocoa

class MainWindowController: NSWindowController, NSWindowDelegate {
    
    static let sharedInstance = MainWindowController()
    
    var mainSplitViewController: MainSplitViewController? = nil
    var mainSidebarViewController: SidebarViewController? = nil
    var mainContentViewController: MainContentViewController? = nil
    var releaseContentViewController: ReleaseViewController? = nil
    var managedObjectContext: NSManagedObjectContext?
    
    @IBOutlet weak var toolbar: NSToolbar!
    
    @IBOutlet weak var addProjectButton: NSToolbarItem!
    
    @IBOutlet weak var addTicketButton: NSToolbarItem!
    
    @IBOutlet weak var addTimeButton: NSToolbarItem!
    
    @IBAction func onRemoveAllItemsButton(_ sender: Any) {
        guard let item = mainSidebarViewController?.outlineView.item(atRow: (mainSidebarViewController?.outlineView.selectedRow)!) else { return }
        let tableViewSelectedRow = mainContentViewController?.timeTableView.clickedRow
        if tableViewSelectedRow != -1 {
            let time = mainContentViewController!.times[tableViewSelectedRow!]
            TimeManager.sharedInstance.deleteTime(time: time)
        } else {
            if let ticket = item as? Ticket {
                TicketManager.sharedInstance.deleteTicket(ticket: ticket)
            } else if let release = item as? Release {
                ReleaseManager.sharedInstance.deleteRelease(release: release)
            }
            NotificationCenter.default.post(name: .reloadReleaseTable, object: managedObjectContext)
        }
        NotificationCenter.default.post(name: .reloadTimeTable, object: managedObjectContext)
        NotificationCenter.default.post(name: .reloadSidebar, object: managedObjectContext)
        NotificationCenter.default.post(name: .reloadStatusbar, object: managedObjectContext)
        enableOrDisableAddButtons()
    }
    
    let appdelegate = NSApplication.shared.delegate as! AppDelegate
    
    var selectedRelease: Release? = nil
    var selectedTicket: Ticket? = nil
    var selectedTime: Time? = nil
    
    var release: Release? = nil
    var ticket: Ticket? = nil
    var time: Time? = nil
    
    
    override func windowDidLoad() {
        super.windowDidLoad()
        self.window?.center()
        let appDelegate = NSApplication.shared.delegate as! AppDelegate
        self.managedObjectContext = appDelegate.persistentContainer.viewContext
        let projectLabel = UserDefaults.standard.string(forKey: "Category") ?? "Release"
        self.addProjectButton.label = "Add \(projectLabel)"
        
        NotificationCenter.default.addObserver(forName: .mainCategoryNameChanged, object: managedObjectContext, queue: OperationQueue.main) { (Notification) in
            let projectLabel = UserDefaults.standard.string(forKey: "Category") ?? "Release"
            self.addProjectButton.label = "Add \(projectLabel)"
        }
        NotificationCenter.default.addObserver(forName: .itemChanged, object: managedObjectContext, queue: OperationQueue.main) { (Notification) in
            self.enableOrDisableAddButtons()
        }
        
        enableOrDisableAddButtons()
        let backgroundColor = NSColor.controlBackgroundColor.cgColor
        let color = CALayer()
        color.backgroundColor = backgroundColor
    
        self.window?.appearance = NSAppearance(named: .darkAqua)
        self.window?.titlebarAppearsTransparent = true
        self.window?.backgroundColor = NSColor(red: 0/255, green: 40/255, blue: 101/255, alpha: 1.0)
        self.window?.contentView?.layer = color

    }
    
    override func windowWillLoad() {
        appdelegate.closePopover(sender: self)
    }
    
    func windowDidBecomeKey(_ notification: Notification) {
        appdelegate.closePopover(sender: self)
    }
    
    func windowDidBecomeMain(_ notification: Notification) {
        appdelegate.closePopover(sender: self)
    }
    
    func enableOrDisableAddButtons() {
        self.addTicketButton.isEnabled = ReleaseManager.sharedInstance.getReleases().count > 0
        self.addTimeButton.isEnabled = TicketManager.sharedInstance.getTickets().count > 0
    }
    
}
