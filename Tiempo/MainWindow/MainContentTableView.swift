//
//  MainContentTableView.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 17.03.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import Cocoa

extension MainContentViewController : NSTableViewDelegate, NSTableViewDataSource {
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let filteredTimes = TimeManager.sharedInstance.getTimesFromTicket(ticket: (mainWindow?.selectedTicket)!)
        
        let currentTime = filteredTimes[row]
        
        var cellText = ""
        var idendifier = ""
        
        if tableColumn == tableView.tableColumns[0] {
            cellText = "\(currentTime.duration)"
            idendifier = "durationCell"
        } else if tableColumn == tableView.tableColumns[1] {
            cellText = currentTime.created ?? ""
            idendifier = "createdCell"
        } else if tableColumn == tableView.tableColumns[2] {
            cellText = currentTime.date ?? ""
            idendifier = "dateCell"
        }
        
        if let cell = tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: idendifier), owner: self) as? NSTableCellView {
            cell.textField?.stringValue = cellText
            return cell
        }
        return nil
    }
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        if (mainWindow?.selectedTicket == nil) {
            return 0
        }
        let filteredTimes = TimeManager.sharedInstance.getTimesFromTicket(ticket: ((mainWindow?.selectedTicket)!)).count
        return filteredTimes
    }
    
    func tableViewSelectionDidChange(_ notification: Notification) {
        guard let tableView = notification.object as? NSTableView else { return }
        if tableView.selectedRow != -1 {
            self.detailsTextView.string = times[tableView.selectedRow].details!
        } else {
            self.detailsTextView.string = ticket!.details!
        }
    }
    
    override func rightMouseDown(with event: NSEvent) {
        let menu = NSMenu()
        if (timeTableView.selectedRow != -1) {
            menu.addItem(withTitle: "Copy duration", action: #selector(onCopyDuration), keyEquivalent: "c")
            menu.addItem(withTitle: "Copy description", action: #selector(onCopyDescription), keyEquivalent: "")
            NSMenu.popUpContextMenu(menu, with: event, for: self.timeTableView)
        }
    }
    
    @objc private func onCopyDuration() {
        pasteboard.clearContents()
        let time = times[timeTableView.selectedRow]
        pasteboard.setString("\(time.duration)", forType: .string)
    }
    
    @objc private func onCopyDescription() {
        pasteboard.clearContents()
        let time = times[timeTableView.selectedRow]
        pasteboard.setString(time.details!, forType: .string)
    }
    
}
