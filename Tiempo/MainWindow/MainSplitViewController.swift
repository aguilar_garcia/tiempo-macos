//
//  MainSplieViewController.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 02.02.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import Cocoa

class MainSplitViewController: NSSplitViewController {
    
    var mainWindowController: MainWindowController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
    override func viewDidAppear() {
        mainWindowController = self.view.window?.windowController as? MainWindowController
        mainWindowController!.mainSplitViewController = self
    }
    
    override func splitView(_ splitView: NSSplitView, shouldHideDividerAt dividerIndex: Int) -> Bool {
        return false
    }
    
}
