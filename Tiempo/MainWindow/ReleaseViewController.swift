//
//  ReleaseViewController.swift
//  Tiempo
//
//  Created by Juan Carlos Aguilar on 03.02.19.
//  Copyright © 2019 Juan Carlos Aguilar. All rights reserved.
//

import Cocoa

class ReleaseViewController: NSViewController, NSTableViewDelegate, NSTableViewDataSource {

    @IBOutlet weak var labelReleaseTimeLogged: NSTextField!
    @IBOutlet weak var releaseTableView: NSTableView!
    
    @IBOutlet weak var labelReleaseName: NSTextField!
    
    var mainWindow: MainWindowController? = nil
    let appDelegate = NSApplication.shared.delegate as! AppDelegate
    var managedObjectContext: NSManagedObjectContext?
    var firstLaunch = true
    var tickets: [Ticket] = []
    var release: Release? = nil
    
    override func viewDidLoad() {
        managedObjectContext = appDelegate.persistentContainer.viewContext
        super.viewDidLoad()
        NotificationCenter.default.addObserver(forName: .reloadReleaseTable, object: managedObjectContext, queue: OperationQueue.main, using: {(notification) in
            self.tickets.removeAll()
            let selectedRelease = self.mainWindow?.selectedRelease
            
            if selectedRelease != nil {
                let filteredTickets = self.filterTickets(release: selectedRelease!)
                
                for ticket in filteredTickets {
                    self.tickets.append(ticket)
                }
                let estimatedTime = ReleaseManager.sharedInstance.getEstimatedTime(selectedRelease!)
                let projectLoggedTime = estimatedTime > 0 ? TimeConverter.sharedInstance.GetStringValueFromTime(time: Int(estimatedTime)) : ""
                self.labelReleaseTimeLogged.stringValue = projectLoggedTime
                
            }
            self.labelReleaseName.stringValue = selectedRelease?.releaseName ?? ""
            self.releaseTableView.reloadData()
            
            print("ReleaseNotification CALLED")
        })
        // Do view setup here.
    }
    
    override func viewDidAppear() {
        self.mainWindow = self.view.window?.windowController as? MainWindowController
        self.mainWindow!.releaseContentViewController = self
        
        releaseTableView.delegate = self
        releaseTableView.dataSource = self
        releaseTableView.reloadData()
        removeTableFromSplitView()
    }
    
    private func filterTickets(release: Release) -> [Ticket] {
        let filteredTickets = TicketManager.sharedInstance.getTickets().filter {
            return ($0 as Ticket).releaseId == mainWindow!.selectedRelease?.releaseId
        }
        return filteredTickets
    }
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        let filteredTickets = TicketManager.sharedInstance.getTickets().filter {
            return ($0 as Ticket).releaseId == mainWindow!.selectedRelease?.releaseId
        }
        return filteredTickets.count
    }

    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        var cellText: String = ""
        var idendifier: String = ""
        let currentTicket = tickets[row]
        
        if tableColumn == tableView.tableColumns[0] {
            cellText = currentTicket.ticketname!
            idendifier = "ticketnameCell"
        } else if tableColumn == tableView.tableColumns[1] {
            let estimatedTime = TicketManager.sharedInstance.getEstimatedTime(currentTicket)
            cellText = estimatedTime == 0 ? "-" : "\(estimatedTime) min"
            idendifier = "trackedCell"
        } else if tableColumn == tableView.tableColumns[2] {
            cellText = currentTicket.time ?? ""
            idendifier = "stampCell"
        } else if tableColumn == tableView.tableColumns[3] {
            cellText = currentTicket.date ?? ""
            idendifier = "dateCell"
        }
        
        if let cell = tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: idendifier), owner: self) as? NSTableCellView {
            cell.textField?.stringValue = cellText
            return cell
        }
        return nil
    }
    
    func tableView(_ tableView: NSTableView, sortDescriptorsDidChange oldDescriptors: [NSSortDescriptor]) {
        print("sortDescriptorsDidChange")        
        let selectedColumn = tableView.tableColumns[tableView.selectedColumn]
        if selectedColumn == tableView.tableColumns[0] {
            self.tickets.sort { $0.ticketname! < $1.ticketname! }
        } else if selectedColumn == tableView.tableColumns[1] {
            self.tickets.sort { $0.duration < $1.duration }
        } else if selectedColumn == tableView.tableColumns[2] {
            self.tickets.sort { $0.time! < $1.time! }
        } else if selectedColumn == tableView.tableColumns[3] {
            self.tickets.sort { $0.date! < $1.date! }
        }
        self.releaseTableView.reloadData()
    }
    
    func tableView(_ tableView: NSTableView, shouldSelectRow row: Int) -> Bool {
        return true
    }
    
    private func removeTableFromSplitView() {
        if firstLaunch {
            self.mainWindow!.mainSplitViewController?.removeSplitViewItem((mainWindow!.mainSplitViewController?.splitViewItems[2])!)
            firstLaunch = false
        }
    }
    
}
