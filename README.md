# Tiempo macOS

#### Copy Tiempo.app into the applications folder to get all updates.

[Download for macOS (Version **2.1**)](https://tiempo.bit-design.org/mac/installer/Tiempo-2.1.zip)

[Download for macOS (Version 1.2)](https://tiempo.bit-design.org/mac/installer/Tiempo-1.2.0.zip)
